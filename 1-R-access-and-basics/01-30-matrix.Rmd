## Matrix

### Create a matrix and list attributes and operations
```{r, echo = TRUE}
A <- matrix(c(5, 4, 9,  1, 4, 5), nrow = 2, ncol = 3, byrow = TRUE,
                  dimnames = list(c("rowA", "rowB"),
                                  c("col01", "col02", "col03")))
A                                  
#       col01 col02 col03
# rowA    5    4    9
# rowB    1    4    5
 ```

### Type of class
```{r, echo = TRUE}
class(A)
# [1] "matrix" "array"
 ```

### Attributes using dim
```{r, echo = TRUE}
dim(A)
# [1] 2 3
 ```

### Attributes using ncol
```{r, echo = TRUE}
ncol(A)
# [1] 3
 ```

### Attributes using nrow
```{r, echo = TRUE}
nrow(A)
# [1] 2
 ```

### Addition
```{r, echo = TRUE}
# add each element of A by 1.9
A + 1.9
#      col01 col02 col03
# rowA   6.9   5.9  10.9
# rowB   2.9   5.9   6.9
 ```

### Substraction
```{r, echo = TRUE}
# substract each element of A by 1.7
A - 1.7
#      col01 col02 col03
# rowA   3.3   2.3   7.3
# rowB  -0.7   2.3   3.3
 ```

### Multiply
```{r, echo = TRUE}
# multiply each element of A by 9.3
A * 9.3
#      col01 col02 col03
# rowA  46.5  37.2  83.7
# rowB   9.3  37.2  46.5
 ```

### Divide
```{r, echo = TRUE}
# multiply each element of A by 100
A / 100
#    col01 col02 col03
# rowA  0.05  0.04  0.09
# rowB  0.01  0.04  0.05
 ```

### Transpose the matrix A
```{r, echo = TRUE}
t(A)
#       rowA rowB
# col01    5    1
# col02    4    4
# col03    9    5
 ```

### Convert the matrix A into a vector
```{r, echo = TRUE}
c(A)
# [1] 5 1 4 4 9 5
 ```

### Alter elements in matrix A
```{r, echo = TRUE}
A[1, 2] <- 8
A
#      col01 col02 col03
# rowA     5     8     9
# rowB     1     4     5
 ```

### Access two elements from matrix where A is a vector
```{r, echo = TRUE}
A[5:6]
# [1] 9 5
 ```

### Access second row of matrix A
```{r, echo = TRUE}
A[2, ]
# col01 col02 col03 
#    1     4     5 
 ```

### Access last column of matrix A
```{r, echo = TRUE}
A[ , 3]
# rowA rowB 
#    9    5
 ```

### Access and zoom into matrix A
```{r, echo = TRUE}
A[ , 2:3]
#      col02 col03
# rowA     8     9
# rowB     4     5
 ```

### Access every second element in A (vector like)
```{r, echo = TRUE}
index <- c(2, 4, 6)
A[index]
# [1] 1 4 5
```

### Creating a matrix by vector combination
```{r, echo = TRUE}
vecA <- c(22.2, 23.1, 33.4, 34.7, 36.7, 40.1, 42.1)
vecB <- c(456, 667, 673, 688, 733, 771, 854)
vecC <- c(0, 1, 2, 0, 1, 1, 2)
#
# using rbind for row binding the vectors
Arbind <- rbind(vecA, vecB, vecC)
Arbind
# [,1]  [,2]  [,3]  [,4]  [,5]  [,6]  [,7]
# vecA  22.2  23.1  33.4  34.7  36.7  40.1  42.1
# vecB 456.0 667.0 673.0 688.0 733.0 771.0 854.0
# vecC   0.0   1.0   2.0   0.0   1.0   1.0   2.0
#
# using cbind for column binding the vectors
Acbind <- cbind(vecA, vecB, vecC)
Acbind
#      vecA vecB vecC
# [1,] 22.2  456    0
# [2,] 23.1  667    1
# [3,] 33.4  673    2
# [4,] 34.7  688    0
# [5,] 36.7  733    1
# [6,] 40.1  771    1
# [7,] 42.1  854    2
#
# print summary for Acbind
summary(Acbind)
 #     vecA            vecB            vecC    
# Min.   :22.20   Min.   :456.0   Min.   :0.0  
# 1st Qu.:28.25   1st Qu.:670.0   1st Qu.:0.5  
# Median :34.70   Median :688.0   Median :1.0  
# Mean   :33.19   Mean   :691.7   Mean   :1.0  
#  3rd Qu.:38.40   3rd Qu.:752.0   3rd Qu.:1.5  
# Max.   :42.10   Max.   :854.0   Max.   :2.0  
 ```

#### References
- https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/matrix accessed: 2024-01-02T15-33-49Z
