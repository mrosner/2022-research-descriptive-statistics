## List of installed packages
```console
Rscript -e "installed.packages()" | cut -d ' ' -f 1-2 > list-of-installed-packages.txt
```

### References
- https://stackoverflow.com/questions/38481980/get-the-list-of-installed-packages-by-user-in-r accessed: 2023-04-24T12-15-13Z


