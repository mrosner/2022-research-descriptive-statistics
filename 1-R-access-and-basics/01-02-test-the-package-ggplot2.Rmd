### Test package ggplot2
#### _Exercise_

Let's test ggplot2 and create a boxplot.
Copy and paste the code to your prompt and Enter.

```{r, echo = TRUE}
# Load ggplot2
library(ggplot2)
 
# The mtcars dataset is natively available
# head(mtcars)

# save the plot as png file to local directory
png(filename = "./01-02-test-package-ggplot2.png")
 
# A really basic boxplot.
ggplot(mtcars, aes(x=as.factor(cyl), y=mpg)) + 
    geom_boxplot(fill="blue", alpha=0.05) + 
    xlab("x as cyl") +
    ylab("y as mpg")

dev.off()
```

Try to study the code above and view the boxplot graphic.

![01-02-test-package-ggplot2.png](./01-02-test-package-ggplot2.png "01-02-test-package-ggplot2.png").


#### Summarize the test
##### _Summary_
Give a summary of what you haved used (One should at least read comments)!

### References (see and compare)
- https://r-graph-gallery.com/262-basic-boxplot-with-ggplot2.html accessed: 2022-11-17T18-55-21Z

