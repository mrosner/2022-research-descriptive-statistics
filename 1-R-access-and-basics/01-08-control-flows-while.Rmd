### While-loop
#### _Goal_
Comprehend the basic structure of while loop.

```{r, echo = TRUE}
counter <- 9
while(counter >= 0) {
  print(counter)
  counter <- counter - 1
}
```

#### _Exercise_
Write your own while loop. Expand the code above and provide additional output using the string 'The counter is: '.

