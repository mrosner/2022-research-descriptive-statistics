
### tolower
```{r echo = TRUE}
# convert string to lowercase
x <- "GRAMMAR IS A GREAT THING TO USE."
tolower(x)
```

#### References
- https://www.statology.org/lowercase-in-r/ accessed: 2023-08-11T05-58-32Z
- https://ladal.edu.au/string.html accessed: 2023-08-11T05-59-05Z
