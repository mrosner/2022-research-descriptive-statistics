
### Numeric
#### _Goal_
What is the data type domain of numeric?

#### Example A
##### _Exercise_
Test by copy and paste or type and enter.
```{r, echo = TRUE}
var <- 17
class(var)
```

#### Example B
##### _Exercise_
Test by copy and paste or type and enter.
```{r, echo = TRUE}
var <- 0
class(var)
```

#### Example C
##### _Exercise_
Test by copy and paste or type and enter.
```{r, echo = TRUE}
numeric <- 47.11
class(numeric)
```

#### Give example
##### _Summary_
- Are the these numerics (provide code as proof):
  + 'A'
  + 5.72
  + TRUE
  + 1.25
  + 23.25
- Give examples for the numeric data type in R (at least five valid).

### References
- https://r-graph-gallery.com/262-basic-boxplot-with-ggplot2.html accessed: 2022-11-17T18-55-21Z
- https://www.w3schools.com/r/r_data_types.asp accessed: 2022-11-23T10-47-43Z
- https://medium.com/trevor-french/data-types-in-r-d67d8af76f2f accessed: 2022-11-23T14-34-03Z
- https://trevorfrench.github.io/R-for-Data-Analysis/p1c3-data-types.html accessed: 2022-11-23T14-37-49Z


