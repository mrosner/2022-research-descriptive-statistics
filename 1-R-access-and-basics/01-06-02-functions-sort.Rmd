## sort
One can sort a given vector by using the sort function, see result.

```{r, echo = TRUE}
vector <- c(-4, 4, -5, 3, -3, 5, 2, 1, -1, 2, 0)
result <- sort(vector)
result
```
