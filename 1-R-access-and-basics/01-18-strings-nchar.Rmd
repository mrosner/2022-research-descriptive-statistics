
### nchar
```{r echo = TRUE}
# count the number of characters (or bytes or width)
x <- "GCGCGGCGACCACGCTGGTGTTCGCTATTTTTGATAATAATCAGCTTTAATGCGTATCCGTTCTTCATCG"
nchar(x)
```

#### References
- https://rdrr.io/r/base/nchar.html accessed: 2023-08-11T06-06-58Z
- https://www.gastonsanchez.com/r4strings/manip.html accessed: 2023-08-11T06-03-29Z
- https://ladal.edu.au/string.html accessed: 2023-08-11T05-59-05Z
- https://cds.cern.ch/record/1338702/files/978-1-4419-0083-8_BookBackMatter.pdf accessed: 2023-08-11T06-41-18Z
